FUNCTION_BLOCK fbDimmer
  VAR_INPUT
           button : bool;
           timer : time;
           
           // �������� ���� (�������/���)
           speed :real;
  END_VAR
  VAR_OUTPUT
            valueReal : real ;
            valueUINT : USINT ;
  END_VAR
  VAR_IN_OUT
  END_VAR
  VAR
     ButtonProcessing : fbButtonProcessing;
     
     // ����� ��� �����'������ �������� �������� ����� (�� ������������ 100.0)
     mem : real := 100;
     
     // ��������� ����� ��� ������������ REAL � USINT
     b : real;
     
     // ����� ������(��������� ��� ���������)
     revers : bool;
     
     Ftrig : F_Trig;
  END_VAR
  VAR_TEMP
  END_VAR
         ButtonProcessing(button := button, timer := timer);
         Ftrig(CLK := button);
         
         // ����� �������� ������ �� ���������� �� ��� ���������� ������
         if Ftrig.Q = true then
            revers := not revers;
         end_if;
         
         // ���� ���� ����� � ������ ����(���������� ������� ������� �������� �����)
         if ButtonProcessing.shortPress then
            if valueReal <> 0.0 then
               mem := valueReal;
               valueReal := 0.0;
               
               // ������������ REAL � USINT
               b := valueReal * 255.0 / 100.0;
               valueUINT := REAL_TO_USINT(b);
            elsif valueReal = 0.0 then
                  valueReal := mem;
                  b := valueReal * 255.0 / 100.0;
                  valueUINT := REAL_TO_USINT(b);
            end_if;
         end_if;
         
         // ����� ��������� ������� ����� �� ��� ��������� ������
         if ButtonProcessing.longPressAndTouch then
         
            // ��������� ��������
            if valueReal >= 100.0 then
               valueReal := 100.0;
               valueUINT := 255;
            end_if;
            
            if valueReal <= 0.0 then
               valueReal := 0.0;
               valueUINT := 0;
            end_if;
            
            // � ��� ��������� ������� ���������� ���������
            if not revers then
               valueReal := valueReal + 0.0055 * speed;
               b := valueReal * 255.0 / 100.0;
               valueUINT := REAL_TO_USINT(b);
            end_if;
            
            // � ��� ��������� ������� ��� �������� 100.0 ���������� ���������
            if revers or valueReal = 100.0 then
               valueReal := valueReal - 0.0055 * speed;
               b := valueReal * 255.0 / 100.0;
               valueUINT := REAL_TO_USINT(b);
            end_if;
         end_if;
END_FUNCTION_BLOCK
