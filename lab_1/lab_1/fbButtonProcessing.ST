FUNCTION_BLOCK fbButtonProcessing
  VAR_INPUT
           button : bool;
           
           // ��������� ������� ����������
           timer : time;
  END_VAR
  VAR_OUTPUT
            pressed,
            released,
            shortPress,
            longPress,
            longPressAndTouch : bool;
  END_VAR
  VAR_IN_OUT
  END_VAR
  VAR

     Rtrig : R_Trig;
     Ftrig : F_Trig;

     timerTON : TON;

     pressedTP,
     releasedTP,
     shortPressTP,
     longPressTP : TP;

     // ����� ��� ������� �������
     start : bool;

     // ��� ���������� ������
     timer_1 : time;

  END_VAR
  VAR_TEMP
  END_VAR
         //�������� ����� TP
         pressedTP.IN := false;
         releasedTP.IN := false;
         shortPressTP.IN := false;
         longPressTP.IN  := false;
         
         Rtrig(CLK := button);
         Ftrig(CLK := button);

         //
         if Rtrig.Q then
            start := true;
         end_if;

         // ������� ������� �� ��� ���������� ������
         if Ftrig.Q then
            start := false;
            longPressAndTouch := false;
            timerTON( IN := start, PT :=T#10s);
         end_if;

         //������ ������� ��� ����� ���� � ���������� ���� ����������
         if start then
            timerTON( IN := start, PT :=T#10s);
            timer_1 := timerTON.ET;
         end_if;

         // ���������� �������-��� �� ��� ��������� ���������� ������
         shortPressTP(IN  := timer_1 > T#0s and timer_1 <= timer and Ftrig.Q, PT := T#1ms,Q => shortPress);

         // ���������� �������-��� �� ��� ��������� ���������� ������
         longPressTP(IN := timer_1 > T#0s and timer_1 >= timer , PT := T#1ms,Q => longPress);

         // ���������� �������-��� �� ��� ��������� ���������� � ��������� ������
         if timer_1 >= timer and start then
            longPressAndTouch := true;;
         end_if;

         // ���������� �������-��� �� ��� ���������� ������
         pressedTP(IN  := Rtrig.Q, PT := T#1ms,Q => pressed);

         // ���������� �������-��� �� ��� ���������� ������
         releasedTP(IN := Ftrig.Q, PT := T#1ms,Q => released);

END_FUNCTION_BLOCK
