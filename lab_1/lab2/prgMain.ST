var_global
          but_1On  {public} at %R0.0 : bool;
          but_2  {public} at %R0.1 : bool;
          but_3Of  {public} at %R0.2 : bool;
end_var
PROGRAM prgMain
  VAR_INPUT
           button : bool;
  END_VAR
  VAR_OUTPUT
  END_VAR
  VAR
     changeLamp : fbChangeLamp;
     allButton : bool;
  END_VAR
  VAR_TEMP
  END_VAR
         but_1On := but_on;
         but_3Of := but_of;
         changeLamp(buttonOn := but_1On, buttonOF := but_3Of, buttonIr := but_2, lamp => lamp);


END_PROGRAM

